
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Firman Ali
 */
public class Bola {

    private int x;
    private int y;
    private Color clr;
    private int diameter;
    private Dimension area;
    private int deltaX;
    private int deltaY;

    public Bola(int x, int y, Color c, int diameter) {
        this.x = x;
        this.y = y;
        this.clr = c;
        this.diameter = diameter;
        //default Dimension ukuran 300x300 (kalian tentukan sendiri)
        area = new Dimension(300, 300);
        this.deltaX = (int) (Math.random() * 20) - 10;
        this.deltaY = (int) (Math.random() * 20) - 10;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setColor(Color c) {
        this.clr = c;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public void setDimension(Dimension d) {
        area = d;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDiameter() {
        return diameter;
    }

    public Color getColor() {
        return clr;
    }

    public Dimension getDimension() {
        return area;
    }

    public void move() {
        if (x <= 0 || x > area.width - diameter) {
            deltaX = -deltaX;
        }
        x = x + deltaX;
        if (y < 0 || y > area.height - diameter) {
            deltaY = -deltaY;
        }
        y = y + deltaY;
    }

    public void Collision(Rectangle kotak) {
        Rectangle kotakygdibanding = new Rectangle(x, y, diameter, diameter);
        if (kotak.intersects(kotakygdibanding)) {

            if (deltaX > 0 && kotakygdibanding.intersectsLine(kotak.getX(), kotak.getY(), kotak.
                    getX(), kotak.getY() + kotak.getHeight())) {
                deltaX = -deltaX;
            } else if (deltaX < 0 && kotakygdibanding.intersectsLine(kotak.getX() + kotak.getWidth(), kotak.getY(), kotak.getX() + kotak.getWidth(), kotak.getY() + kotak.getHeight())) {
                deltaX = -deltaX;
            } else {
                deltaY = -deltaY;
            }
        }
    }

    public void Collision(Bola bola) {
        int jarakX = (bola.getX() + bola.getDiameter() / 2) - (x + diameter / 2);
        int jarakY = (bola.getY() + bola.getDiameter() / 2) - (y + diameter / 2);
        int distance = (int) Math.sqrt(jarakX * jarakX + jarakY * jarakY);
        if (distance < (this.diameter + bola.diameter) / 2) {

            if ((deltaX > 0 && jarakX > 0) || (deltaX < 0 && jarakX < 0)) {
                deltaX = -deltaX;
            }
            if ((deltaY > 0 && jarakY > 0) || (deltaY < 0 && jarakY < 0)) {
                deltaY = -deltaY;
            }
        }
    }

}
