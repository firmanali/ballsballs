
import java.awt.Rectangle;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Firman Ali
 */
public class Kotak extends Rectangle implements Runnable{

    private static int x;
    private static int y;
    private int height;
    private int width;

    public Kotak(int x, int y, int w, int h) {
        super(x,y,w,h);
        this.x = x;
        this.y = y;
        this.height = h;
        this.width = w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setHeight(int h) {
        this.height = h;
    }

    public void setWidth(int w) {
        this.width = w;
    }

//    public int getX() {
//        return this.x;
//    }
//
//    public int getY() {
//        return this.y;
//    }
//
//    public int getHeight() {
//        return this.height;
//    }
//
//    public int getWidth() {
//        return this.width;
//    }

    @Override
    public void run() {
        System.out.println("Test");
    }

}
