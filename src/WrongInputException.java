/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Firman Ali
 */
public class WrongInputException extends Exception{
    public WrongInputException(String message){
        super(message);
        WrongInput wi = new WrongInput();
        wi.setLabel(message);
        wi.show();
    }
}
